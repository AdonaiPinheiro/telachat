import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class MsgItem extends Component {

    constructor(props) {
        super(props);
        this.estilo = styles.balaoEsquerda;
        if(props.data.m) {
            this.estilo = styles.balaoDireita;
        }
    }

    render() {
        return(

            <View style={[styles.balao, this.estilo]}>
                <Text style={styles.nome}>{this.props.data.nome}</Text>
                <Text style={styles.msg}>{this.props.data.msg}</Text>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    balao:{
        backgroundColor:'#FF0000',
        marginLeft:10,
        marginRight:10,
        marginTop:5,
        marginBottom:5,
        padding:10,
        borderRadius:10
    },
    nome:{
        fontSize:16,
        fontWeight:'bold'
    },
    balaoEsquerda:{
        backgroundColor:'#FFF',
        alignSelf:'flex-start',
        marginRight:50
    },
    balaoDireita:{
        backgroundColor:'#4dff4d',
        alignSelf:'flex-end',
        marginLeft:50
    }
});