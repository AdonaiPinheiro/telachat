import React, { Component } from 'react';
import { View, TextInput, StyleSheet, FlatList, ImageBackground } from 'react-native';
import MsgItem from '../src/MsgItem';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            chat:[
                {key:'1', nome:'Adonai', msg:'Oi', m:true},
                {key:'2', nome:'Milena', msg:'Oi, tudo bem?', m:false},
                {key:'3', nome:'Milena', msg:'Quero foder', m:false},
                {key:'4', nome:'Adonai', msg:'Eta carai', m:true},
                {key:'5', nome:'Milena', msg:'Foge não, seu porra! Quero teu pau na minha buceta. NADA DE DIMINUTIVOS', m:false},
                {key:'6', nome:'Adonai', msg:'B=====D---- Na ppk', m:true},
                {key:'7', nome:'Adonai', msg:'Vou te foder harder!', m:true},
                {key:'8', nome:'Milena', msg:'ISSO QUE EU QUERO', m:false},
                {key:'9', nome:'Milena', msg:'SÓ VEM', m:false},
                {key:'10', nome:'Adonai', msg:'#ETACARAI', m:true},
                {key:'11', nome:'Adonai', msg:'Se inscreve no meu canal', m:true},
            ]
        };
    }

    render() {
        return(
            <View style={styles.container}>
                <ImageBackground source={require('../assets/images/bg.jpg')} style={styles.chat}>
                    <FlatList
                        data={this.state.chat}
                        renderItem={({ item }) => <MsgItem data={item} />}
                    />
                    <View style={styles.campoTexto}>
                        <TextInput style={{ flex:10, backgroundColor:'#FFF', padding:10, borderRadius:10, marginRight:10 }} placeholder='Digite sua mensagem...' />
                        <View style={{ width:30, height:30, borderRadius:15, backgroundColor:"#4dff4d", alignItems:'center', justifyContent:'center' }}>
                            <Icon name="arrow-right" size={20} solid color="#FFF"  />
                        </View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    chat:{
        flex:1
    },
    campoTexto:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'transparent',
        margin:10
    }
});