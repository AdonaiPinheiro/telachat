import React, { Component } from 'react';
import { createDrawerNavigator, createAppContainer } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome5'

import Home from './src/Index'

const AppNavgation = createDrawerNavigator({
    MeuAmorDeMinhaVida:{
        screen:Home,
        navigationOptions:{
            drawerIcon: (
                <Icon name="comment-alt" size={17} color="#0066CC" solid />
            )
        }
    }
});

export default createAppContainer(AppNavgation);